#!/bin/bash

echo ### Installing tilde editor...

if ! hash tilde 2>/dev/null ; then
  wget http://os.ghalkes.nl/sources.list.d/install_repo.sh --no-verbose && sudo sh ./install_repo.sh && sudo apt-get --quiet --yes install tilde && rm install_repo.sh
fi

echo ### Installing tilde editor DONE!


echo ### Upgrading PostgreSQL + install PostGIS

sudo apt-get --yes --purge remove postgresql*

sudo rm -r /etc/postgresql/
sudo rm -r /var/lib/postgresql/
sudo userdel postgres

sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update

sudo apt-get --yes --quiet install postgresql-10-postgis-2.4

echo ### Upgrading PostgreSQL + install PostGIS DONE!


echo ### Configuring redis...

sudo sed -i /etc/redis/redis.conf \
  -e 's/^save .*/\# \0/' \
  -e '/# maxmemory .*/a maxmemory 16gb' \
  -e '/# maxmemory-policy .*/a maxmemory-policy allkeys-lru' \

sudo service redis-server restart

echo ### Configuring redis DONE!


echo ### Removing mysql server...
 
sudo apt-get --yes --purge remove mysql-server

sudo rm -rf /var/lib/mysql
sudo rm -rf /var/log/mysql
sudo rm -rf /etc/mysql
sudo userdel -r mysql

echo ### Removing mysql server DONE!


# 
# cleanup
# 
sudo apt-get --yes autoremove
sudo apt-get --yes autoclean
